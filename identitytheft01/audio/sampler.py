import pyo

import identitytheft01.audio as aud
import identitytheft01.audio.utility as autl


class Sampler(pyo.EventInstrument):
    def __init__(self, **args):
        # args: soundfile, offsetfactor, durfactor, pitchfactors, loopsample
        pyo.EventInstrument.__init__(self, **args)

        # compute parameters
        speedinverse = abs(1.0 / self.pitchfactors[0][1])
        file_dur = pyo.sndinfo(self.soundfile)[1]
        offset_dur = file_dur * self.offsetfactor
        snd_dur = (file_dur - offset_dur) * speedinverse
        active_dur = self.dur * self.durfactor
        real_dur = (
            snd_dur if snd_dur < active_dur and self.loopsample is False else active_dur
        )
        fadeout_dur = 0.1 * real_dur

        # unit generators setup
        self.pitch_env = pyo.Linseg(self.pitchfactors, initToFirstVal=True)
        self.amp_env = pyo.Fader(0.00005, fadeout_dur, real_dur)
        self.output = pyo.SfPlayer(
            self.soundfile,
            interp=4,
            mul=self.amp_env,
            speed=self.pitch_env,
            offset=offset_dur,
            loop=self.loopsample,
        )
        # unit generators go
        self.pitch_env.play()
        self.amp_env.play()
        self.output.out()


def render_samples(
    server,
    outfilename,
    outfiledur,
    bpm,
    delays,
    delaysfactor,
    soundfile,
    offsetfactor=0,
    durfactor=1,
    pitchfactors=[[(0, 1)]],
    loopsample=0,
):
    rhy = autl.make_eventseq(delays, delaysfactor)
    server.boot()
    autl.set_filename(server, outfiledur, outfilename)
    e = pyo.Events(
        instr=aud.Sampler,
        bpm=bpm,
        beat=rhy,
        soundfile=soundfile,
        durfactor=durfactor,
        offsetfactor=offsetfactor,
        pitchfactors=pitchfactors,
        loopsample=loopsample,
    ).play()
    # e.sig().out()

    server.start()
    server.shutdown()
