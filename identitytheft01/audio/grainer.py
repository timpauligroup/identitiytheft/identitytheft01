import pyo

import identitytheft01.audio as aud
import identitytheft01.audio.utility as autl
import identitytheft01.general.utility as gutl


class Grainer(pyo.EventInstrument):
    def __init__(self, **args):
        # args: soundfiles start_dens end_dens start_dur end_dur, window
        pyo.EventInstrument.__init__(self, **args)

        # compute parameter
        n_samples = [pyo.sndinfo(soundfile)[0] for soundfile in self.soundfiles]
        n_secs = [pyo.sndinfo(soundfile)[1] for soundfile in self.soundfiles]

        grain_start_dens = gutl.scale_ls(
            n_samples, self.start_dens[0], self.start_dens[1]
        )
        grain_end_dens = gutl.scale_ls(n_samples, self.end_dens[0], self.end_dens[1])
        grain_start_durs = gutl.scale_ls(
            n_samples, self.start_dur[0], self.start_dur[1]
        )
        grain_end_durs = gutl.scale_ls(n_samples, self.end_dur[0], self.end_dur[1])

        # unit generators setup
        self.tables = [pyo.SndTable(soundfile) for soundfile in self.soundfiles]
        self.windows = [
            pyo.LinTable([(0, 1.0), (8191, 0.0)], 8192)
            if self.window == 1
            else pyo.HannTable()
            for t in self.tables
        ]
        self.pos_envs = [
            pyo.Linseg([(0, 1), (self.dur, n)], initToFirstVal=True) for n in n_samples
        ]

        self.dens_envs = [
            pyo.Linseg([(0, start), (self.dur, end)], initToFirstVal=True,)
            for start, end in zip(grain_start_dens, grain_end_dens)
        ]

        self.dur_envs = [
            pyo.Linseg(
                [(0, start * n_sec), (self.dur, end * n_sec)], initToFirstVal=True,
            )
            for start, end, n_sec in zip(grain_start_durs, grain_end_durs, n_secs)
        ]

        self.gran = pyo.Granule(
            self.tables,
            self.windows,
            dens=self.dens_envs,
            pos=self.pos_envs,
            dur=self.dur_envs,
        )

        self.output = pyo.Mix(self.gran, voices=2)
        # unit generators go
        for env in self.pos_envs:
            env.play()
        for env in self.dens_envs:
            env.play()
        for env in self.dur_envs:
            env.play()
        self.output.out()


def render_grains(
    server,
    outfilename,
    outfiledur,
    bpm,
    delays,
    delaysfactor,
    soundfiles,
    start_dens,
    end_dens,
    start_dur,
    end_dur,
    window,
):
    rhy = autl.make_eventseq(delays, delaysfactor)
    server.boot()
    autl.set_filename(server, outfiledur, outfilename)
    e = pyo.Events(
        instr=aud.Grainer,
        bpm=bpm,
        beat=rhy,
        soundfiles=soundfiles,
        start_dens=start_dens,
        end_dens=end_dens,
        start_dur=start_dur,
        end_dur=end_dur,
        window=window,
    ).play()
    # e.sig().out()

    server.start()
    server.shutdown()
