import itertools as it
import os
import pathlib as pl

import identitytheft01.audio as aud
import identitytheft01.general.utility as gutl
import identitytheft01.score.utility as sutl
import fractions


def create_rimshotmadness(server, chain, tempynamic):
    # get the soundfiles

    single_check = sutl.get_single_dur_checker(0, 32, None, False)
    bars = [0.25, 2.8125, 1.0625, 0.25, 0.25]  # hack TODO
    hitfolder = os.getcwd() + "/samples/hit"
    hitfiles = it.cycle(sorted([str(f) for f in pl.Path(hitfolder).glob("*")]))
    dur_start = sorted(chain.keys(), key=lambda key: sum(k[1] for k in key))
    dur_start = filter(lambda x: x[0][1] == 0.125, dur_start)
    duration = 30
    bpm = 85  # hack TODO
    print(bpm)

    for bar, index in zip(bars, it.count(36)):
        whole_check = sutl.get_whole_dur_checker(bar)
        dur_events = gutl.walk_deterministic_until_cond(
            chain, next(dur_start), single_check, whole_check
        )
        rhy = sutl.extend_cut_until(bar, dur_events)
        rhy = [e[1] for e in rhy]
        rhy += [32]
        files = [next(hitfiles) for r in rhy]
        if index == 40:
            files[-1] = hitfolder + "/TM-88_Hits-Goonies-.wav"
        aud.render_samples(
            server=server,
            outfilename=str(index) + "rimshotmadness",
            outfiledur=duration,
            bpm=bpm,
            delays=rhy,
            delaysfactor=1,
            soundfile=files,
        )
