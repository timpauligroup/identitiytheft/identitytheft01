import os
import pathlib as pl

import identitytheft01.audio as aud


def create_showmeyourtriangles(server, tempynamic):
    """no hits"""

    #  get the soundfiles
    folder = os.getcwd() + "/samples/triangle"
    files = [list(sorted([str(f) for f in pl.Path(folder).glob("*")]))]

    duration = 90
    bpm = tempynamic[0]
    print(bpm)
    speedfactor = 1

    aud.render_grains(
        server=server,
        outfilename="2showmeyourtriangles",
        outfiledur=duration,
        bpm=bpm,
        delays=[32],
        delaysfactor=speedfactor,
        soundfiles=files,
        start_dens=[(8, 128)],
        end_dens=[(1, 2)],
        start_dur=[(1 / 32.0, 0.25)],
        end_dur=[(0.5, 1)],
        window=0,
    )
