import os
import pathlib as pl
import itertools as it

import identitytheft01.audio as aud
import identitytheft01.general.utility as gutl
import identitytheft01.score.utility as sutl


def create_morecowbell(server, bars, chain, chain2, tempynamic):
    """end always with cowbell (joke)"""

    # get the soundfiles
    riser_folder = os.getcwd() + "/samples/fx/riser"
    riser_files = sorted([str(f) for f in pl.Path(riser_folder).glob("*")])

    siren_folder = os.getcwd() + "/samples/fx/siren"
    siren_files = sorted([str(f) for f in pl.Path(siren_folder).glob("*")])

    dur_start = sorted(chain.keys(), key=lambda key: sum(k[1] for k in key))[0]
    start2 = sorted(chain2.keys(), key=lambda key: sum(k[1] for k in key))[0]
    single_check = sutl.get_single_dur_checker(0, 32, None, False)

    duration = 10
    bpm = 100  # TODO lots of hacks here
    print(bpm)
    delaysfactor = 1
    durfactor = 1

    durs = []
    files = []
    offsets = []
    pitch_envs = []
    for bar, f, i in zip(
        bars[:-1],
        it.cycle([it.cycle(siren_files), it.cycle(riser_files)]),
        it.count(7, 0.5),
    ):
        whole_check = sutl.get_whole_dur_checker(bar)
        dur_events = gutl.walk_deterministic_until_cond(
            chain, dur_start, single_check, whole_check
        )
        if len(dur_events) >= 2:
            dur_start = (dur_events[-1], dur_events[-2])
        else:
            dur_start = (dur_events[-1], dur_events[-1])
        offset_events = gutl.walk_deterministic_until_cond(
            chain2, start2, single_check, whole_check
        )
        if len(offset_events) >= 2:
            start2 = (offset_events[-1], offset_events[-2])
        else:
            start2 = (offset_events[-1], offset_events[-1])
        tmp_durs = [e[1] for e in dur_events]
        tmp_durs = gutl.stretch(tmp_durs, bar)
        tmp_files = [next(f) for i in range(len(tmp_durs))]
        tmp_offsets = [e[1] - 0.0625 for e in offset_events]
        tmp_pitch_envs = [d + o for d, o in zip(tmp_durs, tmp_offsets)]
        tmp_pitch_envs = [
            -p if e[0] == "kick" or e[0] == "snare" else p
            for p, e in zip(tmp_pitch_envs, dur_events)
        ]
        tmp_pitch_envs = [
            [p for p, i in zip(tmp_pitch_envs, range(r))]
            for r in range(1, len(dur_events) + 1)
        ]
        tmp_pitch_envs = [gutl.scale_ls(env, 1 / bar, bar) for env in tmp_pitch_envs]
        tmp_pitch_env_times = [gutl.stretch(tmp_durs, dur) for dur in tmp_durs]
        tmp_pitch_env_times = [
            gutl.delays_to_times(times) for times in tmp_pitch_env_times
        ]
        tmp_pitch_envs = [
            [(t, p) for p, t in zip(pitches, times)]
            for pitches, times in zip(tmp_pitch_envs, tmp_pitch_env_times)
        ]

        durs.extend(tmp_durs)
        files.extend(tmp_files)
        offsets.extend(tmp_offsets)
        pitch_envs.extend(tmp_pitch_envs)
        if int(i) != i:  # TODO ugly as fuck
            aud.render_samples(
                server=server,
                outfilename=str(int(i - 0.5)) + "morecowbell",
                outfiledur=duration,
                bpm=bpm,
                delays=durs,
                delaysfactor=delaysfactor,
                soundfile=files,
                durfactor=durfactor,
                offsetfactor=offsets,
                pitchfactors=pitch_envs,
                loopsample=1,
            )
            durs = []
            files = []
            offsets = []
            pitch_envs = []
