import os

import pyo


def set_filename(server, dur, filename):
    path = os.getcwd() + "/build/audio/" + filename + ".wav"
    server.recordOptions(dur=dur, filename=path, sampletype=3)


def make_eventseq(values, factor=1):
    ls = [4 * factor * v for v in values]
    return pyo.EventSeq(ls, occurrences=1, stopEventsWhenDone=True)


