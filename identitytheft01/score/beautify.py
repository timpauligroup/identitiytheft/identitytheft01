import abjad as aj


def beautify(staff):
    number_cmd = r"\override TupletNumber.text = #tuplet-number::calc-fraction-text"
    brackets_cmd = r"\override TupletBracket.bracket-visibility = ##t"
    voice_nos = [r"\voiceTwo", r"\voiceOne"]
    # voice_dynamics = [r"\dynamicDown", r"\dynamicUp"]
    for container in staff:
        for voice in container:
            first_leaf = aj.inspect(voice).leaf(0)
            # textlength = aj.LilyPondLiteral(r"\textLengthOn", "before")
            # aj.attach(textlength, first_leaf)

            # brackets tuplet
            brackets_lit = aj.LilyPondLiteral(brackets_cmd, "before")
            aj.attach(brackets_lit, first_leaf)

            # number tuplet
            number_lit = aj.LilyPondLiteral(number_cmd, "before")
            aj.attach(number_lit, first_leaf)

            # engravers
            voice.remove_commands.append("Note_heads_engraver")
            voice.consists_commands.append("Completion_heads_engraver")
            voice.remove_commands.append("Rest_engraver")
            voice.consists_commands.append("Completion_rest_engraver")
            voice.remove_commands.append("Separating_line_group_engraver")
        if len(container) >= 2:
            for voice, voice_no in zip(container, voice_nos):
                first_leaf = aj.inspect(voice).leaf(0)
                aj.attach(aj.LilyPondLiteral(voice_no), voice)
                # aj.attach(aj.LilyPondLiteral(voice_dynamic), voice)
