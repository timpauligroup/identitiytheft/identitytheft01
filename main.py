import pyo

import identitytheft01.audio as aud
import identitytheft01.general.csv as csv
import identitytheft01.general.utility as gutl
import identitytheft01.score as sco
from identitytheft01.score.notemaker import NoteMaker as nm

# dynamics
dynamics = csv.get_events("rhythms", "*_kicksnarehihat.csv")
dynamics = tuple(reversed([[e[2] for e in song] for song in dynamics]))

# tempo
tempi = csv.get_tempi("rhythms", "*_tempo.csv")
tempi = tuple(reversed(tuple(gutl.quantize(t, 5) for t in tempi)))

assert len(dynamics) == len(tempi)
tempynamics = ((t, d) for t, d in zip(tempi, dynamics))

# tape
tape_counter = nm.create_tape_counter()

# datas
kick_chain = csv.create_chain("rhythms", "*_kick.csv")
hihat_chain = csv.create_chain("rhythms", "*_hihat.csv")
kicksnare_chain = csv.create_chain("rhythms", "*_kicksnare.csv")

# kickhihat_chain = csv.create_chain("rhythms", "*_kickhihat.csv")
# snarehihat_chain = csv.create_chain("rhythms", "*_snarehihat.csv")
# kicksnarehihat_chain = csv.create_chain("rhythms", "*_kicksnarehihat.csv")

kick_chain2 = csv.create_chain("rhythms", "*_kick.csv", 2)
kicksnare_chain2 = csv.create_chain("rhythms", "*_kicksnare.csv", 2)
snarehihat_chain2 = csv.create_chain("rhythms", "*_snarehihat.csv", 2)
kicksnarehihat_chain2 = csv.create_chain("rhythms", "*_kicksnarehihat.csv", 2)

kick_chain3 = csv.create_chain("rhythms", "*_kick.csv", 3)
hihat_chain3 = csv.create_chain("rhythms", "*_hihat.csv", 3)
snare_chain3 = csv.create_chain("rhythms", "*_snare.csv", 3)
kickhihat_chain3 = csv.create_chain("rhythms", "*_kickhihat.csv", 3)
kicksnare_chain3 = csv.create_chain("rhythms", "*_kicksnare.csv", 3)
kicksnarehihat_chain3 = csv.create_chain("rhythms", "*_kicksnarehihat.csv", 3)

notes = []

arco_tempynamic = next(tempynamics)
part1 = sco.create_arco(kicksnare_chain, hihat_chain, arco_tempynamic, tape_counter)
notes.append(part1)

sounds_tempynamic1 = next(tempynamics)
sounds_tempynamic2 = next(tempynamics)
part2 = sco.create_showmeyoursounds(
    kicksnare_chain, kick_chain, sounds_tempynamic1, sounds_tempynamic2, tape_counter
)
notes.append(part2)

rolls_tempynamic1 = next(tempynamics)
rolls_tempynamic2 = next(tempynamics)
part3 = sco.create_showmeyourrolls(rolls_tempynamic1, rolls_tempynamic2, tape_counter)
notes.append(part3)

cowbell_tempynamic = next(tempynamics)
part4, cowbell_bars = sco.create_morecowbell(
    kicksnare_chain2, snarehihat_chain2, kick_chain2, cowbell_tempynamic, tape_counter
)
notes.append(part4)

rim_tempynamic = next(tempynamics)
part5 = sco.create_rimshotmadness(
    snare_chain3, kickhihat_chain3, kicksnarehihat_chain3, rim_tempynamic, tape_counter
)
notes.append(part5)

# print the score
# sco.create_score(notes)

snare_chain = csv.create_chain("rhythms", "*_snare.csv")

server = pyo.Server(audio="offline")
# tape1
aud.create_showmeyourkicks(server, kick_chain, sounds_tempynamic1)
# tape2
aud.create_showmeyourtriangles(server, arco_tempynamic)
# tape3
aud.create_showmeyourhihats(server, hihat_chain, sounds_tempynamic2)
# tape4
aud.create_showmeyourhi(server, arco_tempynamic)
# tape5
aud.create_showmeyoursnares(server, snare_chain, rolls_tempynamic1)
# tape6
aud.create_showmeyourclaps(server, rolls_tempynamic1)
# tape7 to tape34
aud.create_morecowbell(
    server, cowbell_bars, kicksnarehihat_chain2, kicksnare_chain2, cowbell_tempynamic
)
# tape 35
aud.create_explosion(
    server,
    kickhihat_chain3,
    kick_chain3,
    snare_chain3,
    hihat_chain3,
    kicksnare_chain3,
    cowbell_tempynamic,
)
# tape 35 to 40
aud.create_rimshotmadness(server, kicksnarehihat_chain3, rim_tempynamic)
