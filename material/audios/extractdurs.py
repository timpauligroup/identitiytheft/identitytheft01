from ADTLib import ADT
import librosa
import os
import csv

# files have to start exactly with first kick on the first beat
# one line is: dur, energy


def quantize(value, quant):
    quantized = quant * round(value / quant)
    result = quantized
    return result


def get_energy(time, audio, sr):
    frame_len = 1024
    pos = round(time * sr)
    frame = audio[pos : (pos + frame_len)]
    energy = librosa.feature.rms(y=frame, frame_length=frame_len, hop_length=frame_len)
    energy = quantize(max(energy[0]), 1 / 128.0)
    return energy


def start(times):
    if times[0][1] > 0:
        return [(times[0][0], 0)] + times
    else:
        return times


def extract_durs(audiofilename, tempo):
    digits = 6
    audio, sr = librosa.load(audiofilename, sr=None)
    audio = librosa.util.normalize(audio)

    drums = ADT([audiofilename], "no", "no")
    kick_times = start([("kick", x) for x in drums[0]["Kick"].tolist()])
    snare_times = start([("snare", x) for x in drums[0]["Snare"].tolist()])
    hihat_times = start([("hihat", x) for x in drums[0]["Hihat"].tolist()])
    kick_snare_times = sorted(snare_times + kick_times, key=lambda x: x[1])
    kick_hihat_times = sorted(hihat_times + kick_times, key=lambda x: x[1])
    snare_hihat_times = sorted(hihat_times + snare_times, key=lambda x: x[1])
    kick_snare_hihat_times = sorted(hihat_times + kick_snare_times, key=lambda x: x[1])

    kick_durs = times_to_durs(kick_times, tempo, audio, sr, digits)
    snare_durs = times_to_durs(snare_times, tempo, audio, sr, digits)
    hihat_durs = times_to_durs(hihat_times, tempo, audio, sr, digits)
    kick_snare_durs = times_to_durs(kick_snare_times, tempo, audio, sr, digits)
    kick_hihat_durs = times_to_durs(kick_hihat_times, tempo, audio, sr, digits)
    snare_hihat_durs = times_to_durs(snare_hihat_times, tempo, audio, sr, digits)
    kick_snare_hihat_durs = times_to_durs(
        kick_snare_hihat_times, tempo, audio, sr, digits
    )

    return (
        kick_durs,
        snare_durs,
        hihat_durs,
        kick_snare_durs,
        kick_hihat_durs,
        snare_hihat_durs,
        kick_snare_hihat_durs,
    )


def times_to_durs(instr_times, tempo, audio, sr, digits):
    instrs = [x[0] for x in instr_times]
    times = [x[1] for x in instr_times]
    energies = [round(get_energy(x, audio, sr), digits) for x in times]
    norm_factor = float(1.0 / (240.0 / tempo))
    normalized_times = [norm_factor * x for x in times]
    quantized_times = [quantize(x, 1 / 32.0) for x in normalized_times]
    loop = zip(quantized_times[1:], quantized_times)
    durs = [round(x - y, digits) for x, y in loop]
    assert len(energies) - 1 == len(durs)
    result = [(i, d, e) for i, d, e in zip(instrs, durs, energies)]

    def dur_filter(x):
        return 0 < x[1]

    return filter(dur_filter, result)


def listtocsv(ls, filename):
    with open(filename, "w") as myfile:
        wr = csv.writer(myfile)
        wr.writerows(ls)


def csvtolist(filename):
    with open(filename, "r") as f:
        reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
        return list(reader)


def process_folder(foldername):
    subfolders = os.walk(foldername)
    next(subfolders)
    for foldername in sorted(subfolders):
        audiofilepath = foldername[0] + "/drums.wav"
        tempofilepath = foldername[0] + "/tempo.txt"
        tempo = csvtolist(tempofilepath)[0][0]
        (
            kick_durs,
            snare_durs,
            hihat_durs,
            kick_snare_durs,
            kick_hihat_durs,
            snare_hihat_durs,
            kick_snare_hihat_durs,
        ) = extract_durs(audiofilepath, tempo)
        listtocsv([(tempo,)], "../../rhythms/" + foldername[0] + "_tempo.csv")
        listtocsv(kick_durs, "../../rhythms/" + foldername[0] + "_kick.csv")
        listtocsv(snare_durs, "../../rhythms/" + foldername[0] + "_snare.csv")
        listtocsv(hihat_durs, "../../rhythms/" + foldername[0] + "_hihat.csv")
        listtocsv(kick_snare_durs, "../../rhythms/" + foldername[0] + "_kicksnare.csv")
        listtocsv(kick_hihat_durs, "../../rhythms/" + foldername[0] + "_kickhihat.csv")
        snarehihat_name = "../../rhythms/" + foldername[0] + "_snarehihat.csv"
        listtocsv(snare_hihat_durs, snarehihat_name)
        kicksnarehihat_name = "../../rhythms/" + foldername[0] + "_kicksnarehihat.csv"
        listtocsv(kick_snare_hihat_durs, kicksnarehihat_name)


process_folder("10essentials_cut")
process_folder("ontherocks_cut")
